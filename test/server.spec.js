const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const PORT = 3000;

describe("Color Code Converter API", () => {
  before("Starting server", (done) => {
    server = app.listen(PORT, () => {
      done();
    });
  });
  describe("RGB to Hex conversion", () => {
    const baseurl = `http://localhost:${PORT}`;
    it("returns status 200", (done) => {
      const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
      request(url, (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });
    it("converts RGB values to hexadecimal", (done) => {
      const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
      request(url, (error, response, body) => {
        expect(body).to.equal("#ff0000");
        done();
      });
    });
    it("handles invalid RGB values", (done) => {
      const url = baseurl + "/rgb-to-hex?r=300&g=100&b=100";
      request(url, (error, response, body) => {
        expect(body).to.equal("Invalid RGB values");
        done();
      });
    });
  });
  describe("Hex to RGB conversion", () => {
    const baseurl = `http://localhost:${PORT}`;
    it("returns status 200", (done) => {
      const url = baseurl + "/hex-to-rgb?hex=%2300ff00";
      request(url, (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });
    it("returns the expected RGB value", (done) => {
      const url = baseurl + "/hex-to-rgb?hex=%2300ff00";
      request(url, (error, response, body) => {
        expect(body).to.equal("rgb(0, 255, 0)");
        done();
      });
    });
    it("returns invalid hexadecimal value for invalid hex input", (done) => {
      const url = baseurl + "/hex-to-rgb?hex=00fg00";
      request(url, (error, response, body) => {
        expect(body).to.equal("Invalid hexadecimal value");
        done();
      });
    });
  });
  after("Stop server", (done) => {
    server.close();
    done();
  });
});
