// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
  describe("RGB to Hex conversions", () => {
    it("converts the basic colors", () => {
      const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
      expect(redHex).to.equal("#ff0000");

      const blueHex = converter.rgbToHex(0, 0, 255); // #ff0000
      expect(blueHex).to.equal("#0000ff");

      const greenHex = converter.rgbToHex(0, 255, 0); // #ff0000
      expect(greenHex).to.equal("#00ff00");
    });
  });

  describe("Hex to RGB conversions", () => {
    it("converts basic hexadecimal values to RGB", () => {
      const redRgb = converter.hexToRgb("#ff0000");
      expect(redRgb).to.equal("rgb(255, 0, 0)");

      const greenRgb = converter.hexToRgb("#00ff00");
      expect(greenRgb).to.equal("rgb(0, 255, 0)");

      const blueRgb = converter.hexToRgb("#0000ff");
      expect(blueRgb).to.equal("rgb(0, 0, 255)");
    });
  });
});
