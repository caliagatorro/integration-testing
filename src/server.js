const express = require("express");
const converter = require("./converter");
const cors = require("cors");
const app = express();
const PORT = 3000;

app.use(cors());

// Welcome endpoint
app.get("/", (req, res) => res.send("Welcome!"));

// RGB to Hex endpoint
app.get("/rgb-to-hex", (req, res) => {
  const red = parseInt(req.query.r);
  const green = parseInt(req.query.g);
  const blue = parseInt(req.query.b);
  const hex = converter.rgbToHex(red, green, blue);
  res.send(hex);
});

// Hex to RGB endpoint
app.get("/hex-to-rgb", (req, res) => {
  const hex = req.query.hex;
  const rgb = converter.hexToRgb(hex);
  res.send(rgb);
});

console.log("NODE_ENV:" + process.env.NODE_ENV);

if (process.env.NODE_ENV === "test") {
  module.exports = app;
} else {
  app.listen(PORT, () => console.log("Server listening..."));
}
